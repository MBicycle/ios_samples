**iOS Samples with SWIFT**


## SPRestAPIManager.swift

This sample of REST API connection manager. It is using third party library Alomofire for SWIFT.
Main methods:

- sendUploadRequest           // Run upload data process
- sendRequest                       // Run GET, POST, PUT, DELETE requests
- handleResponse                 // When Response is received this method handling it and analyzing for any errors or some other messages.
- cancelRequest                    // This method allowing  to cancel current request by assigned url path. Very usefull in context search
- validateResponseOnError   // Check if response with status 200 ok contains some error message.

---

## SPRestAPIManager_Auth.swift

This class is extension from rest api manager and using for auth methods.
In that example we have login, register device token, refresh access token and resent postpone requests.

