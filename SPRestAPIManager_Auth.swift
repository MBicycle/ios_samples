//
//  SPRestAPIManager_Auth.swift
//  SmartPourer
//
//  Created by Ihar Mikalayeu on 8/11/17.
//  Copyright © 2017 Ihar Mikalayeu. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

import Firebase
import FacebookCore
import FacebookLogin
import FBSDKCoreKit
import FBSDKLoginKit

extension SPRestAPIManager {

    // MARK: Authorization
    
    func doLogin(params: [String: String],
                 completion:CompletionBlock?)
    {
        let apiPath = "\(self.SERVER_URL)/oauth/token"
        let tag = REQUEST_TAG.login.rawValue
        self.sendUploadRequest(urlString: apiPath, params: params, completion: { (json, error) in
            if json == nil {
                completion!(nil, error)
                return
            }
            let jsonResult = json as! JSON
            let result = SPAuthModel(json: jsonResult)
            self.authModel = result
            SPUserStorage.setAccessToken(value: result.access_token!)
            completion!(result, error)
        }, requestTag: tag)
    }
    
    // MARK: - Register Device Token
    
    func doRegisterDeviceToken(params: [String: Any],
                               completion:CompletionBlock?)
    {
        let apiPath = "\(self.SERVER_URL)/v1/firebase"
        let tag = REQUEST_TAG.tagsRegisterFCMToken.rawValue
        
        self.sendRequest(urlString: apiPath, params: params, completion: { (json, error) in
            if error == nil {
                if let jsonResult = json as? JSON {
                    completion!(jsonResult, nil)
                }
            } else {
                completion!(nil, error)
            }
        }, method: "POST", urlEncoding: JSONEncoding.default, requestTag: tag)

    }
    
    // MARK: - REFRESH TOKEN
    
    func doRefreshToken(urlString: String!,
                        params: [String : Any]?,
                        completion: CompletionBlock?,
                        method: String,
                        urlEncoding:Alamofire.ParameterEncoding,
                        requestTag: Int)
    {
        Auth.auth().addStateDidChangeListener { auth, user in
            if user != nil {
                user?.getIDToken(completion: { (token, error) in
                    if let error = error {
                        print(error)
                        AppDelegate.sharedInstance.doLogout()
                        return
                    }
                    if FBSDKAccessToken.current() != nil {
                        if let fbToken = FBSDKAccessToken.current().tokenString {
                            self.doRefreshLogin(fcmToken: token!, fbToken: fbToken, completion: { [weak self] (results: Any?, error: NSError?) in
                                if error != nil {
                                    AppDelegate.sharedInstance.doLogout()
                                } else {
                                    self?.sendRequest(urlString: urlString,
                                                      params: params,
                                                      completion: completion,
                                                      method: method,
                                                      urlEncoding: urlEncoding,
                                                      requestTag: requestTag)

                                }
                            })
                        } else {
                            AppDelegate.sharedInstance.doLogout()
                        }
                    } else {
                        AppDelegate.sharedInstance.doLogout()
                    }
                })
            } else {
                AppDelegate.sharedInstance.doLogout()
            }
        }
    }
    
    func doRefreshLogin(fcmToken: String, fbToken: String, completion:CompletionBlock?) {
        let params = ["grant_type" : "fb_token",
                      "fcmtoken"   : fcmToken,
                      "fbtoken"    : fbToken]
        
        self.doLogin(params: params, completion: {(results: Any?, error: NSError?) in
            if error != nil {
                completion!(nil, error!)
                return
            } else {
                completion!(results, nil)
            }
            
        })

    }

}
