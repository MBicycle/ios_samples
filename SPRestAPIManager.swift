//
//  SPRestAPIManager.swift
//  SmartPourer
//
//  Created by Ihar Mikalayeu on 8/11/17.
//  Copyright © 2017 Ihar Mikalayeu. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ObjectMapper
import RealmSwift
import Realm

class SPRestAPIManager: NSObject {

    enum REQUEST_TAG: Int {
        case login = 1,
        tagsRegisterFCMToken,
        clientDrinks,
        clientProfile,
        newsFeed,
        groupList,
        groupInfo,
        groupTimeline,
        updateProfile,
        createGroup,
        drinkList,
        drinkSearchPeople,
        drinkPostUserTag,
        drinkUnits,
        postDeviceDrink,
        searchUser,
        userById,
        tagsPersonal,
        tagsConfirm,
        tagsBottlePair,
        tagsStatistics,
        timeToDriveLimit,
        setBacLimit,
        getBacLimit,
        searchCoctails
    }
    
    // SINGLETON
    static let sharedManager = SPRestAPIManager()
    
    // OTHER VARIABLES
    typealias CompletionBlock = (_ results: Any?, _ error: NSError?) -> Void
    
    let SERVER_URL = "https://spourer.com"
    
    var authModel: SPAuthModel?
    var currentUserProfile: SPProfileModel?
    
    // MARK: SENT REQUEST

    func sendUploadRequest(urlString: String!,
                              params: [String : String]?,
                          completion: CompletionBlock?,
                          requestTag: Int)
    {
        var logString = "============= Upload Data Request ===========\n"
        logString = logString.appending("URL: \(urlString)\n")
        
        let headers = ["Authorization" : "Basic YW5kcm9pZC1hbGNvOjEyMzQ1Ng==",
                       "Cache-Control" : "no-cache"]
        
        logString = logString.appending("Headers :\n")
        for (key, value) in headers {
            logString = logString.appending("\(key): \(value)\n")
        }
        logString = logString.appending("\n")
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in params! {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                logString = logString.appending("Content-Disposition: form-data; name=\(key)\n")
                logString = logString.appending("\(value)\n")
                logString = logString.appending("\(multipartFormData.boundary)\n")
            }
            logString = logString.appending("\n")
            print(logString)
        }, to:urlString,
           method: .post,
           headers: headers)
        { [weak self] result in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                })
                
                upload.validate()
                
                upload.responseJSON { response in
                    if (response.response != nil) {
                        self?.logResponse(response.response!, response.data)
                    } else {
                        print(response.result.error?.localizedDescription)
                        
                    }
                    self?.handleResponse(response: response,
                                         completion: completion,
                                         requestTag: requestTag,
                                         urlString: urlString,
                                         method: "POST",
                                         urlEncoding: JSONEncoding.default,
                                         params: nil)

                }
                
            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }

    
    func sendRequest(urlString: String!,
                     params: [String : Any]?,
                     completion: CompletionBlock?,
                     method: String,
                     urlEncoding:Alamofire.ParameterEncoding,
                     requestTag: Int)
    {
        
        let headers = self.headers()
        let httpMethod = HTTPMethod(rawValue: method)!
        
        let dataRequest: DataRequest = Alamofire.request(urlString, method: httpMethod, parameters: params, encoding: urlEncoding, headers: headers)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON
            { [weak self] response in
                
                if (response.response != nil) {
                    self?.logResponse(response.response!, response.data)
                }
                self?.handleResponse(response: response,
                                     completion: completion,
                                     requestTag: requestTag,
                                     urlString: urlString,
                                     method: method,
                                     urlEncoding: urlEncoding,
                                     params: params)
                
        }
        if dataRequest.request != nil {
            self.logRequest(dataRequest.request!, params: params)
        }
    }

    // MARK: - HANDLE REQUEST
    
    func handleResponse(response: DataResponse<Any>,
                        completion: CompletionBlock?,
                        requestTag: Int,
                        urlString: String!,
                        method: String,
                        urlEncoding:Alamofire.ParameterEncoding,
                        params: [String : Any]?)
    {

        if response.response == nil {
            if requestTag == REQUEST_TAG.drinkList.rawValue ||
               requestTag == REQUEST_TAG.drinkSearchPeople.rawValue ||
               requestTag == REQUEST_TAG.searchCoctails.rawValue
            {
                if self.isNoInternetConnection(response: response) {
                    completion!(nil, self.error("No internet connection", code: -1009))
                }
                return
            } else {
                if self.isNoInternetConnection(response: response) {
                    completion!(nil, self.error("No internet connection", code: -1009))
                    return
                }
                completion!(nil, self.error("Uknown server error: 500", code: 500))
                return
            }
        }
        let statusCode = response.response?.statusCode
        
        let headers = response.response?.allHeaderFields as! [String : Any]
        
        let ttdString = headers["ttd"] as? String
        let bac = headers["bac"] as? Double
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let ttd = formatter.date(from: ttdString ?? "")
        
        
        if let currentObject = SPCurrentStateRealmModel.getCurrentObject() {
            currentObject.updateObject(timeToDrive: ttd, bac: bac)
        } else {
            _ = SPCurrentStateRealmModel(timeToDrive: ttd, bac: bac)
        }
        
        if statusCode == nil {
            completion!(nil, self.error("Uknown server error", code: 500))
            return
        }
        if requestTag != REQUEST_TAG.tagsRegisterFCMToken.rawValue {
            let error = self.validateResponseOnError(response.data)
            if error != nil && statusCode != 401 {
                completion!(nil, error)
                return
            }
        }
        
        if statusCode == 200
        {
            let json = JSON(data: response.data!)
            completion!(json, nil)
        }
        else if statusCode == 204 {
            let headers = response.response?.allHeaderFields
            completion!(headers, nil)
        }
        else if statusCode == 201 {
            let headers = response.response?.allHeaderFields
            completion!(headers, nil)
        }
        else if statusCode == 404
        {
            completion!(nil, self.error("Ups.. Server is temporary unavailable. Please try later", code: 404))
        }
        else if statusCode == 400
        {
            completion!(nil, self.error("Server error: 400 bad request", code: 400))
        }
        else if statusCode == 422
        {
            completion!(nil, self.error("Server error: 400 bad request", code: 400))
        }
        else if statusCode == 401
        {
            self.cancelAllRequests()
            self.doRefreshToken(urlString: urlString,
                                params: params,
                                completion: completion,
                                method: method,
                                urlEncoding: urlEncoding,
                                requestTag: requestTag)
        }
        else if statusCode == 502
        {
            completion!(nil, self.error("Ups.. Server is temporary unavailable. Please try later", code: 502))
        } else {
            completion!(nil, self.error("Uknown server error: 500", code: 500))
        }
    }
    
    func isNoInternetConnection(response: DataResponse<Any>) -> Bool {
        var isConnected = false
        switch response.result {
        case .failure(let error):
            if let respError = error as? NSError {
                let code = respError.code
                if code == -1009 {
                    isConnected = true
                }
            }
            
        default:
            break
        }
        return isConnected
    }
    
    //MARK: - REQUESTS CANCELATION
    
    func cancelRequest(requestPath: String) {
        let cancelationUrl: URL? = URL(string: requestPath)
        if cancelationUrl != nil {
            let lastPath = cancelationUrl?.lastPathComponent
            Alamofire.SessionManager.default.session.getAllTasks { (tasks) in
                tasks.forEach({task in
                    if task.currentRequest?.url?.lastPathComponent == lastPath
                    {
                        task.cancel()
                    }
                })
            }
    }
    }
    
    func cancelAllRequests() {
        Alamofire.SessionManager.default.session.getAllTasks { (tasks) in
            
            tasks.forEach({$0.cancel()})
        }
    }
    
    // MARK: Headers
    
    func headers() -> [String : String] {
        var _headers = [String : String]()
        _headers["Cache-Control"] = "no-cache"
        _headers["Content-Type"] = "application/json;charset=UTF-8"
        if let auth = self.authModel {
            _headers["Authorization"] = "Bearer \(auth.access_token!)"
        }
        return _headers
    }
    
    // MARK: Error Handling
    
    func validateResponseOnError(_ data: Data?) -> NSError? {
        
        do {
            let errorObj = try JSONSerialization.jsonObject(with: data!, options: [])
            if let jsonEror: NSDictionary = (errorObj as? NSDictionary) {
                if let status = jsonEror["status"] as? Int {
                    if status == 200 {
                        return nil
                    } else if let errorMsg = jsonEror["message"] as? String {
                        return self.error(errorMsg, code: 400)
                    }
                }
                else if let errorMsg = jsonEror["error_description"] as? String {
                    return self.error(errorMsg, code: 401)
                }
                else if let errorMsg = jsonEror["message"] as? String {
                    if let errorCode = jsonEror["errorCode"] as? Int {
                        return self.error(errorMsg, code: errorCode)
                    } else {
                        return self.error(errorMsg, code: 400)
                    }
                }
            }
            return nil
        } catch {
            print("json error: \(error)")
            if let errorData = data {
                let errorString = NSString(data: errorData, encoding: String.Encoding.utf8.rawValue)
                print(errorString)
            }
            return nil
        }
    }
    
    func error(_ errorMsg: String, code: Int) -> NSError {
        let userInfo: [AnyHashable: Any] =
            [
                NSLocalizedDescriptionKey :  NSLocalizedString("Unauthorized", value: errorMsg, comment: ""),
                NSLocalizedFailureReasonErrorKey : NSLocalizedString("Unauthorized", value: errorMsg, comment: "")
        ]
        let err = NSError(domain: "Domain", code: code, userInfo: userInfo)
        return err
    }

    
    // MARK: Logging
    
    func logRequest(_ request: URLRequest, params: [String : Any]?) {
        let method = request.httpMethod
        let urlString = request.url?.absoluteString
        
        print("\(method!) \(urlString!)")
        
        let headers = request.allHTTPHeaderFields
        self.logHeaders(headers!)
        
        //        if let httpBody = request.httpBody {
        //            let httpBodyStr = NSString(data: httpBody, encoding: String.Encoding.utf8.rawValue)
        //            print("\(httpBodyStr!)")
        //        }
        
        if let postParams = params {
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: postParams, options: JSONSerialization.WritingOptions.prettyPrinted)
                let datastring = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)
                print("Post body : \n \(datastring!)")
                // here "jsonData" is the dictionary encoded in JSON data
            } catch let error as NSError {
                print(error)
            }
        }
    }
    
    func logResponse(_ response: URLResponse, _ data: Data?) {
        if let url = response.url?.absoluteString {
            print("\n Response: \(url)")
        }
        if let httpResponse = response as? HTTPURLResponse {
            let localisedStatus = HTTPURLResponse.localizedString(forStatusCode: httpResponse.statusCode).capitalized
            print("Status: \(httpResponse.statusCode) - \(localisedStatus)")
        }
        
        if let headers = (response as? HTTPURLResponse)?.allHeaderFields as? [String: AnyObject] {
            self.logHeaders(headers)
        }
        
        if (data?.count)! > 0 {
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                let pretty = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                if let string = NSString(data: pretty, encoding: String.Encoding.utf8.rawValue) {
                    print("JSON: \(string)")
                }
            }
                
            catch {
                if let string = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) {
                    print("Data: \(string)")
                }
            }
        }
    }
    
    func logHeaders(_ headers: [String: Any]) {
        print("Headers: [")
        for (key, value) in headers {
            print("  \(key) : \(value)")
        }
        print("]")
    }


    func doLogout() {
        self.cancelAllRequests()
        self.authModel = nil
        self.currentUserProfile = nil
    }

}
